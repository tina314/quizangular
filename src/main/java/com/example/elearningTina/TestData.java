package com.example.elearningTina;

import com.example.elearningTina.model.Answer;
import com.example.elearningTina.model.Question;
import com.example.elearningTina.model.QuestionType;
import com.example.elearningTina.model.Test;
import com.example.elearningTina.service.AnswerService;
import com.example.elearningTina.service.QuestionService;
import com.example.elearningTina.service.TestService;
//import com.example.elearningTina.service.TestSolutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class TestData {

    @Autowired
    AnswerService answerService;

    @Autowired
    QuestionService questionService;

    @Autowired
    TestService testService;

//    @Autowired
//    TestSolutionService testSolutionService;

    @PostConstruct
    public void init() {
        Question q1 = new Question("Salvador Dali's muse was called?", 10, QuestionType.RADIO_BUTTON);

        q1 = questionService.save(q1);

        Answer a1 = new Answer("Lana", q1, false);
        Answer a2 = new Answer("Lara", q1, false);
        Answer a3 = new Answer("Sara", q1, false);
        Answer a4 = new Answer("Gala", q1, true);

        a1 = answerService.save(a1);
        a2 = answerService.save(a2);
        a3 = answerService.save(a3);
        a4 = answerService.save(a4);

        ArrayList<Answer> answers = new ArrayList<>();
        answers.add(a1);
        answers.add(a2);
        answers.add(a3);
        answers.add(a4);

        q1.setAnswers(answers);

        q1 = questionService.save(q1);


        Question w1 = new Question("How many championships has Lewis Hamilton won?", 5, QuestionType.RADIO_BUTTON);

        w1 = questionService.save(w1);

        Answer t1 = new Answer("1", w1, false);
        Answer t2 = new Answer("4", w1, false);
        Answer t3 = new Answer("6", w1, true);
        Answer t4 = new Answer("7", w1, false);

        t1 = answerService.save(t1);
        t2 = answerService.save(t2);
        t3 = answerService.save(t3);
        t4 = answerService.save(t4);

        ArrayList<Answer> answersAgain = new ArrayList<>();
        answers.add(t1);
        answers.add(t2);
        answers.add(t3);
        answers.add(t4);

        w1.setAnswers(answersAgain);

        w1 = questionService.save(w1);

//        List<Question> questions = new ArrayList<>();
//        questions.add(q1);
//        questions.add(w1);

        Test test1 = new Test("Naslov Prvog Testa");
//
        test1 = testService.save(test1);
        test1.addQuestion(q1);
        test1.addQuestion(w1);
        test1 = testService.save(test1);

//        //        test1.setQuestions(questions);
//
//        test1 = testService.save(test1);

    }

}
