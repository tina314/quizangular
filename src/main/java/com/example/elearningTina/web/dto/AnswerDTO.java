package com.example.elearningTina.web.dto;

import com.example.elearningTina.model.Answer;

import java.util.List;
import java.util.stream.Collectors;

public class AnswerDTO {

    private Long id;

    private String answer;

    private Long questionId;

    private boolean correct;

    AnswerDTO() {}

    private AnswerDTO(Long id, String answer, Long questionId, boolean correct) {
        this.id = id;
        this.answer = answer;
        this.questionId = questionId;
        this.correct = correct;
    }

    public Long getId() {
        return id;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public static AnswerDTO from(Answer a) {
        return new AnswerDTO(a.getId(), a.getAnswer(), a.getQuestion().getId(), a.isCorrect());
    }

    public static List<AnswerDTO> from(List<Answer> answers) {
        return answers.stream().map(answer -> from(answer)).collect(Collectors.toList());
    }


    public static class AnswerDTOBuilder {

        private Long id;

        private String answer;

        private Long questionId;

        private boolean correct;

        public AnswerDTOBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public AnswerDTOBuilder withAnswer(String answer) {
            this.answer = answer;
            return this;
        }

        public AnswerDTOBuilder withQuestionId(Long questionId){
            this.questionId = questionId;
            return this;
        }

        public AnswerDTOBuilder isCorrect(boolean correct){
            this.correct = correct;
            return this;
        }

        public AnswerDTO build() {
            AnswerDTO dto = new AnswerDTO();

            validate();

            dto.answer = this.answer;
            dto.correct = this.correct;
            dto.id = this.id;
            dto.questionId = this.questionId;

            return dto;
        }

        private void validate () {
            if (this.answer == null) {
                throw new RuntimeException("Answer is null!");
            }
            if (this.id == null) {
                throw new RuntimeException("Id is null!");
            }
            if (this.questionId == null) {
                throw new RuntimeException("Question id is null!");
            }

        }
    }
}
