package com.example.elearningTina.web.dto;

import com.example.elearningTina.model.Test;

import java.util.ArrayList;
import java.util.List;

public class TestDTO {

    private Long id;

    private String title;

    private List<QuestionDTO> questions = new ArrayList<>();

    public TestDTO() {}

    public TestDTO(Long id, String title, List<QuestionDTO> questions) {
        this.id = id;
        this.title = title;
        this.questions = questions;
    }

    public TestDTO(String title, List<QuestionDTO> questions) {
        this.title = title;
        this.questions = questions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }

    public static TestDTO from (Test test) {
        return new TestDTO(test.getId(), test.getTitle(), QuestionDTO.from(test.getQuestions()));
    }
//    public static QuestionDTO from(Question q) {
//        return new QuestionDTO(q.getId(), q.getQuestionText(), q.getPoints(), q.getQuestionType(), AnswerDTO.from(q.getAnswers()));
//    }
}
