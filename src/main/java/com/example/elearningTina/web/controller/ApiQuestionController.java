package com.example.elearningTina.web.controller;

import com.example.elearningTina.model.Answer;
import com.example.elearningTina.model.Question;
import com.example.elearningTina.service.AnswerService;
import com.example.elearningTina.service.QuestionService;
import com.example.elearningTina.web.dto.AnswerDTO;
import com.example.elearningTina.web.dto.ConverterDTO;
import com.example.elearningTina.web.dto.QuestionDTO;
import com.example.elearningTina.web.util.PaginationUtil;
import org.apache.tomcat.util.http.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/questions")
@CrossOrigin(origins = "*")
public class ApiQuestionController {

    @Autowired
    QuestionService questionService;

    @Autowired
    AnswerService answerService;

    @Autowired
    ConverterDTO converterDTO;

//    RADIII ALI BEZ PAGINACIJE
//    @RequestMapping(method = RequestMethod.GET)
//    ResponseEntity<List<QuestionDTO>> getQuestions() {
//
//        List<QuestionDTO> questions = questionService.findAll().stream().map(QuestionDTO::from).collect(Collectors.toList());
//
//        return new ResponseEntity<>(questions, HttpStatus.OK);
//    }

//    SONJA PROVERA
//    @GetMapping
//    public ResponseEntity<Page<QuestionDTO>> findAllPageable(
//            @PageableDefault(page = 0, size = 3)
////            @RequestParam(value="filter", required = false)
////                    Optional<String> filter,
//                    Pageable pageable) {
////        if(filter.isPresent()) {
////            return ResponseUtil.page(productService.findByFilter(filter.get(), pageable));
////        }
////        return new ResponseEntity<>(questionService.findAll(pageable), HttpStatus.OK);
//          //return questionService.findAll(pageable);
//        Page<QuestionDTO> page = questionService.findAll(pageable);
//        return ResponseEntity.ok()
//                .headers()
//                .body(questionService.findAll(pageable).getContent());
//    }

    // SONJA PROVERA 2
    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<QuestionDTO>> getQuestions(Pageable pageable) {
        Page<Question> questions = questionService.findAll(pageable);
        questions.getContent().stream().map(question-> QuestionDTO.from(question)).collect(Collectors.toList());
        Page<QuestionDTO> questionDTOS = new PageImpl<>(
                questions.getContent().stream()
                        .map(q -> QuestionDTO.from(q))
                        .collect(Collectors.toList()),
                pageable,
                questions.getTotalElements());
        return page(questionDTOS);   //  ResponseUtil.page(questionDTOS);
    }

    static <T> ResponseEntity<List<T>> page(Page<T> page) {
        return ResponseEntity.ok()
                .headers(PaginationUtil.getPageResponseHeaders(page))
                .body(page.getContent());
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    ResponseEntity<QuestionDTO> getQuestionWithAnswers(@PathVariable Long id) {

        //  Optional<Question> question = Optional.of(new Question());

        Optional<Question> question = questionService.findOne(id);

        if (question.isEmpty()) {
            throw new RuntimeException("There is no question with the given ID");
        }

//        Question question = questionService.getById(id);

        return new ResponseEntity<>(QuestionDTO.from(question.get()), HttpStatus.OK);
    }

//      Ovu smo prebacili u AnswersController
//    @RequestMapping(value= "/{id}/answers", method = RequestMethod.GET)
//    ResponseEntity<List<AnswerDTO>> getAnswersOfQuestion(@PathVariable Long id) {
//
//        List<AnswerDTO> answers = AnswerDTO.from(questionService.getById(id).getAnswers());
//
//        return new ResponseEntity<>(answers, HttpStatus.OK);
//
//    }

    // RADI ALI BEZ ODGOVORA
//    @RequestMapping(method= RequestMethod.POST,
//            consumes="application/json")
//    public ResponseEntity<QuestionDTO> createQuestion(
//            @RequestBody QuestionDTO newQestionDTO){  // @Validated ce trebati!!
//
//        Question question = converterDTO.toEntityQuestionWithoutId(newQestionDTO);
//
//        question = questionService.save(question);
//
//        return new ResponseEntity<>(
//                QuestionDTO.from(question),
//                HttpStatus.CREATED);
//    }

    // DA LI TREBA @TRANSACTIONAL
    @RequestMapping(method = RequestMethod.POST,
            consumes = "application/json")
    public ResponseEntity<QuestionDTO> createQuestionWithAnswers(
            @RequestBody QuestionDTO newQestionDTO) {  // @Validated ce trebati!!

        List<AnswerDTO> answerDTOS = newQestionDTO.getAnswers();
        // AnswersDTO stizu sa fronta i nemaju questionId!!!!!!!!!!1

        Question question = converterDTO.toEntityQuestionWithoutId(newQestionDTO);

        question = questionService.save(question);  //ovde dobijam questionId!!!

        for (AnswerDTO answerDto: answerDTOS) {
            answerDto.setQuestionId(question.getId());
        }

        List<Answer> answers = converterDTO.toListOfAnswerEntitiesWithId(answerDTOS);
//        List<Answer> answers = converterDTO.toListOfAnswerEntitiesWithId(answerDTOS);

        answers = answerService.save(answers); // tu ili ispod
        //question.setAnswers(answers);   // probaj u petlji sa addAnswer

        for (Answer a: answers) {
            question.addAnswer(a);
        }

        question = questionService.save(question);
        //ovde dodaj odgovore na pitanje koje ima id jer je bilo u bazi
//        answer = answerService.save(answer);
//        question.get().addAnswer(answer);
//        questionService.save(question.get());

        return new ResponseEntity<>(
                QuestionDTO.from(question),
                HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
    public ResponseEntity<QuestionDTO> editQuestionWithAnswers(@RequestBody QuestionDTO questionDto,
                                                               @PathVariable Long id) {

        if (!id.equals(questionDto.getId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
      Question persisted = questionService.edit(questionDto);

        return new ResponseEntity<>(QuestionDTO.from(persisted), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    ResponseEntity<QuestionDTO> delete(@PathVariable Long id) {

        Question deleted = questionService.delete(id);

        return new ResponseEntity<>(QuestionDTO.from(deleted), HttpStatus.OK);
    }

}
