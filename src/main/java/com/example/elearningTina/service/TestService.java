package com.example.elearningTina.service;

import com.example.elearningTina.model.Answer;
import com.example.elearningTina.model.Test;

import java.util.List;

public interface TestService {

    Test findOne(Long id);


    List<Test> findAll();


    Test save(Test test);


    List<Test> save(List<Test> test);


    void delete(Long id);
}
